package Class;


import Class.Math.Linear.Point;
import Class.Math.Linear.Vector;

public class Space {

    private int mXSize;
    private int mYSize;
    private int mZSize;

    private static ColorPoint[][][] m3DPointSpaceArray;

    public Space(int XSize, int YSize, int ZSize){
        build3DPointSpaceArray(XSize, YSize, ZSize);
    }

    private void build3DPointSpaceArray(int XSize, int YSize, int ZSize){
        mXSize = XSize;
        mYSize = YSize;
        mZSize = ZSize;

        m3DPointSpaceArray = new ColorPoint[XSize][YSize][ZSize];

    }

    public void drawPoint(Point Point, ColorPoint Color){
        m3DPointSpaceArray[Point.getX()][Point.getY()][Point.getZ()] = Color;
    }

    public void deletePoint(Point Point){
        m3DPointSpaceArray[Point.getX()][Point.getY()][Point.getZ()] = null;
    }

    public void sendRayThereAndBackAgain(Ray Ray){
        Point vectorPointer = new Point(Ray.getStartPoint());
        Vector grownVector = Ray.getVector();

        int counter = 0;
        while(!isInsideSpace(vectorPointer) && counter < 50){ //TODO 50
            vectorPointer.growByVector(grownVector);

            counter++;
        }

        if(counter >= 50){
            return;
        }

        while(isInsideSpace(vectorPointer)){
            int x = vectorPointer.getX();
            int y = vectorPointer.getY();
            int z = vectorPointer.getZ();

            if(m3DPointSpaceArray[x] != null && m3DPointSpaceArray[x][y] != null && m3DPointSpaceArray[x][y][z] != null){
                Ray.setBackAgainFeedBack(vectorPointer, m3DPointSpaceArray[x][y][z]);
                break;
            }

            vectorPointer.growByVector(grownVector);
        }

        return;
    }

    private boolean isInsideSpace(Point Point){
        int x = Point.getX();
        int y = Point.getY();
        int z = Point.getZ();

        if(x < 0 || y < 0 || z < 0) {
            return false;
        }

        if(x >= mXSize || y >= mYSize || z >= mZSize){
            return false;
        }

        return true;
    }

}
