package Class;

import Class.Math.Linear.Point;
import Class.Math.Linear.Vector;
import Class.Math.Math;

public class Ray {

    private Point mStartPoint;
    private Vector mVector;
    private Point mIntersectsPoint;

    private int mDistanceIntersectsToStart;
    private ColorPoint mColorPoint;

    public Ray (Point StartPoint, Vector Vector){
        setStartPoint(new Point(StartPoint));
        setVector(Vector);

    }

    public void setIntersectsPoint(Point IntersectsPoint) {
        this.mIntersectsPoint = IntersectsPoint;
    }

    private void setVector(Vector mVector) {
        this.mVector = mVector;
    }

    private void setStartPoint(Point mStartPoint) {
        this.mStartPoint = mStartPoint;
    }

    public void setBackAgainFeedBack(Point TouchPoint, ColorPoint Color){
        mColorPoint = Color;
        mIntersectsPoint = TouchPoint;
        mDistanceIntersectsToStart = Math.squareRoot(
                Math.squarePower(mIntersectsPoint.getX() - mStartPoint.getX()) +
                Math.squarePower(mIntersectsPoint.getY() - mStartPoint.getY()) +
                Math.squarePower(mIntersectsPoint.getZ() - mStartPoint.getZ())
        );
    }

    public Point getStartPoint() {
        return mStartPoint;
    }

    public Vector getVector() {
        return mVector;
    }

    public ColorPoint getColorPoint() {
        return mColorPoint;
    }

    @Override
    public String toString() {
        return "StartP: x=" + Integer.toString(mStartPoint.getX()) + " y=" + Integer.toString(mStartPoint.getY()) + " z=" + Integer.toString(mStartPoint.getZ());
    }
}
