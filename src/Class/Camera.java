package Class;

import Class.Math.Linear.Plane;
import Class.Math.Linear.Point;
import Class.Math.Linear.Vector;
import Class.Math.MathR;
import Class.Math.VectorPackage;

public class Camera {
    private int mXSize;
    private int mYSize;

    private Point mCameraPoint;
    private Point mCameraUpperRightEdge;
    private Vector mCameraVector;
    private Ray[][] mRaysArray;

    private Space mSpace;

    private VectorPackage mVectorPackage;

    public Camera(int XSize, int YSize, Point CameraPoint, VectorPackage VectorPackage, Vector CameraVector){
        buildRaysArray(
                (XSize % 2) == 0 ? XSize + 1 : XSize,
                (YSize % 2) == 0 ? YSize + 1 : YSize
        );

        setCameraPoistion(CameraPoint, CameraVector, VectorPackage);

        buildRays();
    }

    private void setSizes(int XSize, int YSize) {
        this.mXSize = XSize;
        this.mYSize = YSize;

    }

    public int[][] getColorMap(){
        int[][] colorMap = new int[mXSize][mYSize];

        for (int i = 0; i < mXSize; i++) {
            for (int j = 0; j < mYSize; j++) {
                mSpace.sendRayThereAndBackAgain(mRaysArray[i][j]);
                ColorPoint colorPoint = mRaysArray[i][j].getColorPoint();
                if(colorPoint != null){
                    colorMap[i][j] = colorPoint.getRGB();
                }
            }
        }

        return colorMap;
    }

    public void setSpace(Space Space){
        mSpace = Space;
    }

    public void buildRays(){
        int helfX = (mXSize - 1) / 2,
            helfY = (mYSize - 1) / 2;

        for(int i = 0; i < mXSize; i++){
            for (int j = 0; j < mYSize; j++) {
                mRaysArray[i][j] = new Ray(
                        getPointOn(mCameraPoint, i, j, helfX, helfY),
                        mCameraVector);
            }
        }
    }

    public Point getPointOn(Point MiddlePoint, int i, int j, int HelfX, int HelfY){
        Point point = new Point(MiddlePoint);
        if(i != HelfX){
            point.growByVector(i < HelfX ? mVectorPackage.getLeftVector() : mVectorPackage.getRightVector(), MathR.abs(i - HelfX));
        }

        if(j != HelfY){
            point.growByVector(j < HelfY ? mVectorPackage.getDownVector() : mVectorPackage.getUpVector(), MathR.abs(j - HelfY));
        }

        return point;
    }

    public void setCameraPoistion(Point CameraPoint, Vector CameraVector, VectorPackage VectorPackage) {
        this.mCameraPoint = CameraPoint;
        this.mCameraVector = CameraVector.setGCF();
        this.mVectorPackage = VectorPackage;

    }

    private void buildRaysArray(int Xsize, int Ysize){
        setSizes(Xsize, Ysize);
        mRaysArray = new Ray[Xsize][Ysize];

    }

    public Ray[][] getRays(){
        return mRaysArray;
    }

}
