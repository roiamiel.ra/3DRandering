package Class;

import Class.Math.Linear.*;
import Class.Math.Linear.Point;
import Class.Math.Linear.Vector;
import Class.Math.VectorPackage;
import Class.Space;
import Class.ColorPoint;
import Class.Camera;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public static final int CANVAS_WIDTH = 100;
    public static final int CANVAS_HEIGHT = 100;

    private DrawCanvas canvas;

    public Frame() {
        canvas = new DrawCanvas();
        canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));

        Container cp = getContentPane();
        cp.add(canvas);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 300);
        setVisible(true);
    }

    private class DrawCanvas extends JPanel {
        // Override paintComponent to perform your own painting
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.BLACK);

            Space space = new Space(100, 100, 100);

            ColorPoint[] colors = {
                    new ColorPoint(Color.BLUE.getRGB()),    //left
                    new ColorPoint(Color.RED.getRGB()),     //right
                    new ColorPoint(Color.GREEN.getRGB()),   //bottom
                    new ColorPoint(Color.YELLOW.getRGB()),  //top
                    new ColorPoint(Color.CYAN.getRGB()),    //front
                    new ColorPoint(Color.MAGENTA.getRGB())  //back
            };

            Point cameraPoint = new Point(-1, -1, -1);
            Camera camera = new Camera(200, 200, cameraPoint, new VectorPackage(new Vector(1, 1, 0), new Vector(0, 1, 1)), new Vector(cameraPoint, new Point(0, 0, 0)));
            camera.setSpace(space);

            int x, y, z;

            //left
            x = 0;
            for(y = 0; y < 100; y++){
                for(z = 0; z < 100; z++){
                    space.drawPoint(new Point(x, y, z), colors[0]);
                }
            }

            //right
            x = 99;
            for(y = 0; y < 100; y++){
                for(z = 0; z < 100; z++){
                    space.drawPoint(new Point(x, y, z), colors[1]);
                }
            }

            //bottom
            y = 0;
            for(x = 0; x < 100; x++){
                for(z = 0; z < 100; z++){
                    space.drawPoint(new Point(x, y, z), colors[2]);
                }
            }

            //top
            y = 99;
            for(x = 0; x < 100; x++){
                for(z = 0; z < 100; z++){
                    space.drawPoint(new Point(x, y, z), colors[3]);
                }
            }

            //front
            z = 0;
            for(x = 0; x < 100; x++){
                for(y = 0; y < 100; y++){
                    space.drawPoint(new Point(x, y, z), colors[4]);
                }
            }

            //back
            z = 99;
            for(x = 0; x < 100; x++){
                for(y = 0; y < 100; y++){
                    space.drawPoint(new Point(x, y, z), colors[5]);
                }
            }

            clearScreen();
            long secTime = System.currentTimeMillis();

            int counter = 0;

            while(true){
                long start = System.currentTimeMillis();
                int[][] colorMap = camera.getColorMap();

                if(start >= secTime + 1000){
                    clearScreen();
                    System.out.println("fps: " + counter);
                    secTime = System.currentTimeMillis();
                    counter = 0;
                } else {
                    counter++;
                }

                for (int i = 0; i < colorMap.length; i++) {
                    for (int j = 0; j < colorMap[i].length; j++) {
                        g.setColor(new Color(colorMap[i][j]));
                        g.drawLine(i, j, i, j);
                    }
                }

                break;
            }
        }

        public void clearScreen() {
            for (int i = 0; i < 50; i++){
                System.out.println("");
            }
        }

    }
}