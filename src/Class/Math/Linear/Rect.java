package Class.Math.Linear;

import com.sun.istack.internal.NotNull;

public class Rect {

    private Point[] mPointsArray;

    public Rect(@NotNull Point A, @NotNull Point B, @NotNull Point C, @NotNull Point D){
        setmPointsArray(new Point[]{A, B, C, D});

    }

    public void setmPointsArray(Point[] mPointsArray) {
        this.mPointsArray = mPointsArray;
    }

    public Point[] getmPointsArray() {
        return mPointsArray;
    }

    public Point getA(){
        return mPointsArray[0];
    }

    public Point getB(){
        return mPointsArray[1];
    }

    public Point getC(){
        return mPointsArray[2];
    }

    public Point getD(){
        return mPointsArray[3];
    }

}
