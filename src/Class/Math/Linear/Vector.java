package Class.Math.Linear;

import Class.Math.Math;

public class Vector extends Point {

    public Vector (int X, int Y, int Z) {
        setX(X);
        setY(Y);
        setZ(Z);
    }

    public Vector (Point PointA, Point PointB){
        int aX = PointA.getX();
        int bX = PointB.getX();

        int aY = PointA.getY();
        int bY = PointB.getY();

        int aZ = PointA.getZ();
        int bZ = PointB.getZ();

        setX(bX > aX ? Math.abs(bX - aX) : bX - aX);
        setY(bY > aY ? Math.abs(bY - aY) : bY - aY);
        setZ(bZ > aZ ? Math.abs(bZ - aZ) : bZ - aZ);

    }

    public Vector getInverse(){
        return new Vector(getX() * -1, getY() * -1, getZ() * -1);
    }

    public Vector setGCF(){
        int gCF = Math.getGCF(getX(), getY(), getZ());
        setX(getX() / gCF);
        setY(getY() / gCF);
        setZ(getZ() / gCF);

        return this;
    }

    public Vector add(Vector Vector){
        return new Vector(getX() + Vector.getX(), getY() + Vector.getY(), getZ() + Vector.getZ());
    }

    public Vector sub(Vector Vector){
        return new Vector(getX() - Vector.getX(), getY() - Vector.getY(), getZ() - Vector.getZ());
    }

    public Vector multiply(Vector Vector){
        return new Vector(getX() * Vector.getX(), getY() * Vector.getY(), getZ() * Vector.getZ());
    }

    public Vector division(Vector Vector){
        return new Vector(getX() / Vector.getX(), getY() / Vector.getY(), getZ() / Vector.getZ());
    }

    public Vector abs(){
        return new Vector(Math.abs(getX()), Math.abs(getY()), Math.abs(getZ()));
    }

}
