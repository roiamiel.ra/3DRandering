package Class.Math.Linear;

public class Point {

    private int mX;
    private int mY;
    private int mZ;

    public Point(){

    }

    public Point(int X, int Y, int Z){
        setX(X);
        setY(Y);
        setZ(Z);
    }

    public Point(Point Point){
        setX(Point.getX());
        setY(Point.getY());
        setZ(Point.getZ());
    }

    public void growByVector(Vector Vector){
        mX += Vector.getX();
        mY += Vector.getY();
        mZ += Vector.getZ();
    }

    public void growByVector(Vector Vector, int times){
        mX += Vector.getX() * times;
        mY += Vector.getY() * times;
        mZ += Vector.getZ() * times;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public int getZ() {
        return mZ;
    }

    public void setX(int X) {
        this.mX = X;
    }

    public void setY(int Y) {
        this.mY = Y;
    }

    public void setZ(int Z) {
        this.mZ = Z;
    }
}
