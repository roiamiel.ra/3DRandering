package Class.Math.Linear;

public class Plane {

    private int mA;
    private int mB;
    private int mC;
    private int mD;

    private Vector mUP;
    private Vector mDown;
    private Vector mRight;
    private Vector mLeft;

    private Point mCenterPoint;

    public Plane(Point CenterPoint, Vector Vector){
        setA(Vector.getX());
        setB(Vector.getY());
        setC(Vector.getZ());

        setD(mA * CenterPoint.getX() + (mB * CenterPoint.getY()) + (mC * CenterPoint.getZ()));

        setCenterPoint(CenterPoint);

        buildVectors();
    }

    private void buildVectors(){
        int x = (mCenterPoint.getX() + 1);
        int y = mCenterPoint.getY();
        int z = mC != 0 ? (mD - (mA * x + mB * y)) / mC : mCenterPoint.getZ();

        mUP = new Vector(mCenterPoint, new Point(x, y, z));
        mDown = mUP.getInverse();

        y = mCenterPoint.getY() + 1;
        z = mCenterPoint.getZ();
        x = mA != 0 ? (mD - (mB * y + mC * z)) / mA : mCenterPoint.getX();

        mRight = new Vector(mCenterPoint, new Point(x, y, z));
        mLeft = mRight.getInverse();

    }

    public Point getPointBy(int x, int y){
        Point point = new Point(mCenterPoint);

        if(x > 0){
            point.growByVector(mRight, x);

        } else if(x < 0){
            point.growByVector(mLeft, x);
        }

        if(y > 0){
            point.growByVector(mUP, y);

        } else if(y < 0){
            point.growByVector(mDown, y);
        }

        return point;
    }

    private void setCenterPoint(Point CenterPoint) {
        this.mCenterPoint = CenterPoint;
    }

    private void setA(int A) {
        this.mA = A;
    }

    private void setB(int B) {
        this.mB = B;
    }

    private void setC(int C) {
        this.mC = C;
    }

    private void setD(int D) {
        this.mD = D;
    }
}
