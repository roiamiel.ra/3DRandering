package Class.Math;

public class Factor {
    private int mFactor;

    public Factor (int Value){
        setFactor(Value);
    }

    public void setFactor(int mFactor) {
        this.mFactor = mFactor;
    }

    public int getFactor() {
        return mFactor;
    }

}
