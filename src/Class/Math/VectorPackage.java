package Class.Math;

import Class.Math.Linear.Vector;

public class VectorPackage {
    private Vector mUpVector;
    private Vector mLeftVector;

    private Vector mDownVector;
    private Vector mRightVector;

    public VectorPackage(Vector UpVector, Vector mLeftVector){
        setUpVector(UpVector.setGCF());
        setLeftVector(mLeftVector.setGCF());

        setDownVector(mUpVector.getInverse());
        setRightVector(mLeftVector.getInverse());

    }

    private void setUpVector(Vector UpVector) {
        this.mUpVector = UpVector;
    }

    private void setLeftVector(Vector LeftVector) {
        this.mLeftVector = LeftVector;
    }

    private void setDownVector(Vector DownVector) {
        this.mDownVector = DownVector;
    }

    private void setRightVector(Vector RightVector) {
        this.mRightVector = RightVector;
    }

    public Vector getUpVector() {
        return mUpVector;
    }

    public Vector getLeftVector() {
        return mLeftVector;
    }

    public Vector getDownVector() {
        return mDownVector;
    }

    public Vector getRightVector() {
        return mRightVector;
    }
}
