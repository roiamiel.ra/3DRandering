package Class.Math;

/**
 * @author Roi Amiel (roiamiel.ra@gmail.com)
 * @version 1.0
 * Date 11/08/16
 *
 */
public abstract class MathR {

    /**
     * Integer Absolute Function
     * @param Num Number Of Type Integer
     * @return The Absolute Value Of The Param "Num"
     *
     */
    public static int abs(int Num){
        return (Num < 0) ? -Num : Num;
    }

    /**
     * Double Absolute Function
     * @param Num Number Of Type Double
     * @return The Absolute Value Of The Param "Num"
     *
     */
     public static double abs(double Num){
        return (Num < 0) ? -Num : Num;
    }

    /**
     * Float Absolute Function
     * @param Num Number Of Type Float
     * @return The Absolute Value Of The Param "Num"
     *
     */
    public static float abs(float Num){
        return (Num < 0) ? -Num : Num;
    }

    /**
     * Short Absolute Function
     * @param Num Number Of Type Short
     * @return The Absolute Value Of The Param "Num"
     *
     */
    public static short abs(short Num){
        return (Num < 0) ? (short) -Num : Num;
    }

    /**
     * Get Integer Average Of Integer Array Function
     * @param NumArray Array Of Integer Numbers That Take A Part In Average Value
     * @param ArraySize The Size Of The "NumArray"
     * @return The Integer Average Of All The Numbers In NumArray
     *
     */
    public static int getAverage(int[] NumArray, int ArraySize){
        int counter = 0;
        for(int i = 0; i < ArraySize; i++){
            counter += NumArray[i];
        }

        return counter / ArraySize;
    }

    /**
     * Get Double Average Of Double Array Function
     * @param NumArray Array Of Double Numbers That Take A Part In Average Value
     * @param ArraySize The Size Of The "NumArray"
     * @return The Double Average Of All The Numbers In NumArray
     *
     */
    public static double getAverage(double[] NumArray, int ArraySize){
        int counter = 0;
        for(int i = 0; i < ArraySize; i++){
            counter += NumArray[i];
        }

        return counter / ArraySize;
    }

    /**
     * Get Float Average Of Float Array Function
     * @param NumArray Array Of Float Numbers That Take A Part In Average Value
     * @param ArraySize The Size Of The "NumArray"
     * @return The Float Average Of All The Numbers In NumArray
     *
     */
    public static float getAverage(float[] NumArray, int ArraySize){
        int counter = 0;
        for(int i = 0; i < ArraySize; i++){
            counter += NumArray[i];
        }

        return counter / ArraySize;
    }

    /**
     * Get Character Average Of Character Array Function
     * @param NumArray Array Of Character Numbers That Take A Part In Average Value
     * @param ArraySize The Size Of The "NumArray"
     * @return The Character Average Of All The Numbers In NumArray
     *
     */
    public static char getAverage(char[] NumArray, int ArraySize){
        int counter = 0;
        for(int i = 0; i < ArraySize; i++){
            counter += NumArray[i];
        }

        return (char) (counter / ArraySize);
    }

    /**
     * Get Short Average Of Short Array Function
     * @param NumArray Array Of Short Numbers That Take A Part In Average Value
     * @param ArraySize The Size Of The "NumArray"
     * @return The Short Average Of All The Numbers In NumArray
     *
     */
    public static short getAverage(short[] NumArray, int ArraySize){
        int counter = 0;
        for(int i = 0; i < ArraySize; i++){
            counter += NumArray[i];
        }

        return (short) (counter / ArraySize);
    }

    /**
     * Round Integer Number To The Most Close Integer Precision Function
     * @param Num The Integer Number That Gone Round
     * @param Precision The Integer Precision That Gone Round The Number
     * @return The Integer Of Most Close Multiple Of "Precision" To The "Num"
     *
     */
    public static int round(int Num, int Precision){
        int firstOps = Precision * ((int) (Num / Precision)), secondOps = firstOps + Precision;

        return abs(Num - firstOps) <= abs(Num - secondOps) ? firstOps : secondOps;
    }

    /**
     * Round Double Number To The Most Close Double Precision Function
     * @param Num The Double Number That Gone Round
     * @param Precision The Double Precision That Gone Round The Number
     * @return The Double Of Most Close Multiple Of "Precision" To The "Num"
     *
     */
    public static double round(double Num, double Precision){
        double firstOps = Precision * ((int) (Num / Precision)), secondOps = firstOps + Precision;

        return abs(Num - firstOps) <= abs(Num - secondOps) ? firstOps : secondOps;
    }

    /**
     * Round Float Number To The Most Close Float Precision Function
     * @param Num The Float Number That Gone Round
     * @param Precision The Float Precision That Gone Round The Number
     * @return The Float Of Most Close Multiple Of "Precision" To The "Num"
     *
     */
    public static float round(float Num, float Precision){
        float firstOps = Precision * ((int) (Num / Precision)), secondOps = firstOps + Precision;

        return abs(Num - firstOps) <= abs(Num - secondOps) ? firstOps : secondOps;
    }

    /**
     * Round Short Number To The Most Close Short Precision Function
     * @param Num The Short Number That Gone Round
     * @param Precision The Short Precision That Gone Round The Number
     * @return The Short Of Most Close Multiple Of "Precision" To The "Num"
     *
     */
    public static short round(short Num, short Precision){
        short firstOps = (short) (Precision * (Num / Precision)), secondOps = (short) (firstOps + Precision);

        return abs(Num - firstOps) <= abs(Num - secondOps) ? firstOps : secondOps;
    }

    /**
     * Get Integer Square Of Num
     * @param Num Some Integer Number
     * @return The Integer Square Of "Num"
     *
     */
    public static int squarePower(int Num){
        return Num * Num;
    }

    /**
     * Get Double Square Of Num
     * @param Num Some Double Number
     * @return The Double Square Of "Num"
     *
     */
    public static double squarePower(double Num){
        return Num * Num;
    }

    /**
     * Get Float Square Of Num
     * @param Num Some Float Number
     * @return The Float Square Of "Num"
     *
     */
    public static float squarePower(float Num){
        return Num * Num;
    }

    /**
     * Get Integer Square Root Of Number
     * @param Num Integer Number To Root
     * @return The Integer Square Root Of "Num"
     *
     */
    //TODO Create root method insted of Class.Math.sqrt
    public static int squareRoot(int Num){
        return (int) java.lang.Math.sqrt(Num);
    }

    /**
     * Get Double Square Root Of Number
     * @param Num Double Number To Root
     * @return The Double Square Root Of "Num"
     *
     */
    //TODO Create root method insted of Class.Math.sqrt
    public static double squareRoot(double Num){
        return java.lang.Math.sqrt(Num);
    }

    /**
     * Get Float Square Root Of Number
     * @param Num Float Number To Root
     * @return The Float Square Root Of "Num"
     *
     */
    //TODO Create root method insted of Class.Math.sqrt
    public static float squareRoot(float Num){
        return (float) java.lang.Math.sqrt(Num);
    }

    /**
     * Return The Larger Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Larger Number
     */
    public static short max(short a, short b) {
        return (a >= b) ? a : b;
    }

    /**
     * Return The Larger Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Larger Number
     */
    public static byte max(byte a, byte b) {
        return (a >= b) ? a : b;
    }

    /**
     * Return The Larger Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Larger Number
     */
    public static int max(int a, int b) {
        return (a >= b) ? a : b;
    }

    /**
     * Return The Larger Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Larger Number
     */
    public static float max(float a, float b) {
        return (a >= b) ? a : b;
    }

    /**
     * Return The Larger Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Larger Number
     */
    public static double max(double a, double b) {
        return (a >= b) ? a : b;
    }

    /**
     * Return The Lower Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Lower Number
     */
    public static byte min(byte a, byte b) {
        return (a <= b) ? a : b;
    }

    /**
     * Return The Lower Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Lower Number
     */
    public static short min(short a, short b) {
        return (a <= b) ? a : b;
    }

    /**
     * Return The Lower Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Lower Number
     */
    public static int min(int a, int b) {
        return (a <= b) ? a : b;
    }

    /**
     * Return The Lower Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Lower Number
     */
    public static float min(float a, float b) {
        return (a <= b) ? a : b;
    }

    /**
     * Return The Lower Number
     * @param a Num Number 1
     * @param b Num Number 2
     * @return The Lower Number
     */
    public static double min(double a, double b) {
        return (a <= b) ? a : b;
    }

}
