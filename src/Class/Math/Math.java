package Class.Math;

import java.util.ArrayList;

public abstract class Math extends MathR {

    public static final float mTwoSquareRoot = 1.4142135623F;

    public static int getGCF(int A, int B, int C){
        Factor[][] factors = new Factor[][]{
                getFactorsOf(A),
                getFactorsOf(B),
                getFactorsOf(C)
        };

        int[] sizes = new int[]{
                factors[0].length,
                factors[1].length,
                factors[2].length
        };

        int minIndex = 0, minIndexValue = sizes[0];
        for (int i = 1; i < sizes.length; i++) {
            if(sizes[i] < minIndexValue){
                minIndex = i;
                minIndexValue = sizes[i];
            }
        }

        ArrayList<Factor> finalFactors = new ArrayList<>(minIndexValue);
        for(int i = 0; i < sizes[minIndex]; i++){
            int thisFactor = factors[minIndex][i].getFactor();

            boolean found = true;
            for(int j = 0; j < factors.length && found; j++){
                if(j == minIndex){
                    continue;
                }

                for(int z = 0; z < factors[j].length; z++){
                    Factor zIndexFactor = factors[j][z];
                    if(zIndexFactor == null){
                        continue;
                    }

                    found = zIndexFactor.getFactor() == thisFactor;

                    if(found){
                        factors[j][z] = null;
                        break;
                    }
                }
            }

            if(found){
                finalFactors.add(new Factor(thisFactor));
            }
        }

        int finalSum = 1;
        for (Factor finalFactor : finalFactors) {
            finalSum *= finalFactor.getFactor();
        }

        return finalSum;
    }

    public static Factor[] getFactorsOf(int Num){
        if(Num == 0){
            return new Factor[] {new Factor(0)};
        }

        ArrayList<Factor> factors = new ArrayList<>(8);

        if(Num < 0){
            factors.add(new Factor(-1));

            Num *= -1;
        }

        if(Num == 1){
            if(factors.size() == 1){
                return (Factor[]) factors.toArray();

            } else {
                return new Factor[] {new Factor(1)};

            }
        }

        for (int i = 2; i <= Num / i; i++) {
            while (Num % i == 0) {
                factors.add(new Factor(i));
                Num /= i;

            }
        }

        if (Num > 1) {
            factors.add(new Factor(Num));
        }

        return factors.toArray(new Factor[]{});
    }
}
