package Class;

public class ColorPoint {
    private int mRGB;

    public ColorPoint (int RGB){
        this.setRGB(RGB);
    }

    public void setRGB(int mRGB) {
        this.mRGB = mRGB;
    }

    public int getRGB() {
        return mRGB;
    }
}
